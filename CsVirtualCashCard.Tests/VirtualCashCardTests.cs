using System;
using System.Linq;
using System.Security.Authentication;

using FluentAssertions;

using Xunit;

namespace CsVirtualCashCard.Tests
{
    /// <summary>
    /// Here we only test the constraints on the constructor and the methods
    /// exposed by the IVirtualCashCard interface.
    /// </summary>
    public class VirtualCashCardTests
    {
        private readonly VirtualCashCard _card;
        private const string DefaultPin = "1234";
        private const int DefaultInitialBalance = 10_000;

        public VirtualCashCardTests()
        {
            _card = new VirtualCashCard(DefaultPin);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("abc")]
        [InlineData("abcd")]
        [InlineData("456987")]
        [InlineData("4569 7894")]
        [InlineData("4569" + "\\r\\n" + "7894")]
        public void Constructor_should_fail_on_invalid_pins(string invalidPin)
        {
            new Action(() => new VirtualCashCard(invalidPin)).Should().Throw<ArgumentException>();
        }

        [Theory]
        [InlineData("1234")]
        [InlineData("4569")]
        [InlineData("0156")]
        public void Constructor_should_suceed_on_valid_pins(string validPin)
        {
            new Action(() => new VirtualCashCard(validPin)).Should().NotThrow();
        }

        [Fact]
        public void Constructor_should_initialise_balance_to_zero()
        {
            _card.Balance.Should().Be(0);
        }

        [Fact]
        public void TopUp_should_increase_the_balance_on_the_card()
        {
            var topUpAmount = 10;

            var previousBalance = _card.Balance;
            _card.TopUp(topUpAmount);

            _card.Balance.Should().Be(previousBalance + topUpAmount);
        }

        [Fact]
        public void TopUp_should_increase_the_balance_when_called_in_parallel()
        {
            var topUpAmount = 1;
            var topUpCount = 20000;

            var startBalance = _card.Balance;

            Enumerable.Repeat(topUpAmount, topUpCount)
                .AsParallel()
                .ToList()
                .ForEach(amount => _card.TopUp(amount));

            _card.Balance.Should().Be(startBalance + topUpAmount * topUpCount);
        }

        [Fact]
        public void TopUp_should_not_change_the_balance_when_overflow_occurs()
        {
            var startBalance = _card.Balance;

            var topUpAmount = decimal.MaxValue;
            _card.TopUp(topUpAmount);
            _card.Balance.Should().Be(startBalance + topUpAmount);

            _card.TopUp(0.01m);
            _card.Balance.Should().Be(startBalance + topUpAmount);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-10)]
        [InlineData(-0.0000001)]
        public void TopUp_should_throw_on_negative_amounts(decimal negativeAmount)
        {
            new Action(() => _card.TopUp(negativeAmount)).Should().Throw<ArgumentException>();
        }

        private void TopUpCardForWithdrawTest()
        {
            _card.TopUp(DefaultInitialBalance);
            _card.Balance.Should().Be(DefaultInitialBalance);
        }

        [Theory]
        [InlineData("4567")]
        [InlineData("7894")]
        [InlineData("{0}")]
        public void Withdraw_should_throw_on_incorrect_pin(string wrongPin)
        {
            TopUpCardForWithdrawTest();
            wrongPin.Should().NotBe(DefaultPin);
            
            new Action(() => _card.Withdraw(wrongPin, 1)).Should().Throw<InvalidCredentialException>();
        }

        [Theory]
        [InlineData(-2)]
        [InlineData(-2540)]
        [InlineData(-0.0000002)]
        public void Withdraw_should_throw_on_negative_amounts(decimal amount)
        {
            TopUpCardForWithdrawTest();
            
            new Action(() => _card.Withdraw(DefaultPin, amount)).Should().Throw<ArgumentException>();
        }

        [Fact]
        public void Withdraw_should_succeed_on_valid_pin_and_amounts()
        {
            TopUpCardForWithdrawTest();

            var withdrawAmount = 500m;
            withdrawAmount.Should().BeLessOrEqualTo(DefaultInitialBalance, "otherwise the test is not valid.");

            _card.Withdraw(DefaultPin, withdrawAmount);

            _card.Balance.Should().Be(DefaultInitialBalance - withdrawAmount);
        }

        [Fact]
        public void Withdraw_should_not_withdraw_if_funds_are_insufficient()
        {
            TopUpCardForWithdrawTest();

            var withdrawAmount = 20_000m;
            withdrawAmount.Should().BeGreaterThan(DefaultInitialBalance, "otherwise the test is not valid.");

            _card.Withdraw(DefaultPin, withdrawAmount);

            _card.Balance.Should().Be(DefaultInitialBalance, "the balance should not change");
        }

        [Fact]
        public void Withdraw_should_decrease_the_balance_when_called_in_parallel()
        {
            TopUpCardForWithdrawTest();

            var withdrawAmount = 19;
            var withdrawCount = 10_000;
            Enumerable.Repeat(withdrawAmount, withdrawCount)
                .AsParallel()
                .ToList()
                .ForEach(amount => _card.Withdraw(DefaultPin, amount));

            _card.Balance.Should().Be(DefaultInitialBalance % 19);
        }
    }
}
