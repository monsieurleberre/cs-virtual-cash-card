﻿using System;
using System.Security.Authentication;
using System.Text.RegularExpressions;

namespace CsVirtualCashCard
{
    public class VirtualCashCard : IVirtualCashCard
    {
        private const string pinRegexPattern = @"^[\d]{4}$";

        private static readonly Regex _pinRegex = new Regex(pinRegexPattern);

        private readonly string _pin;
        private readonly object _balanceLock = new object();

        /// <summary>
        /// Constructor for the VirtualCashCard, can be used to create
        /// a new card with a given <see cref="pin"/> and 0 initial balance.
        /// </summary>
        /// <param name="pin">The PIN for the card, it can not be changed and
        /// is expected to be a 4 digits code.</param>
        public VirtualCashCard(string pin)
        {
            if(string.IsNullOrEmpty(pin)) throw new ArgumentNullException(nameof(pin));
            if(!_pinRegex.IsMatch(pin)) throw new ArgumentException(
                $"Pin {pin} invalid. The pin for the card should be composed of 4 digits only.");
            _pin = pin;
        }

        /// <inheritdoc />
        public decimal Balance { get; private set; }

        /// <inheritdoc />
        public void TopUp(decimal amount)
        {
            if(amount < 0) throw new ArgumentOutOfRangeException($"{nameof(amount)} cannot be negative.");
            try
            {
                lock (_balanceLock)
                {
                    var newBalance = Balance + amount;
                    Balance = newBalance;
                }
            }
            catch (OverflowException)
            {
                //We'd probably warn the user about the failure in real life...
            }
        }

        /// <inheritdoc />
        public void Withdraw(string pin, decimal amount)
        {
            if(pin != _pin) throw new InvalidCredentialException("Incorrect PIN provided");
            if (amount < 0) throw new ArgumentOutOfRangeException($"{nameof(amount)} cannot be negative.");

            lock (_balanceLock)
            {
                var newBalance = Balance - amount;
                if (newBalance < 0) return;
                Balance = newBalance;
            }
        }
    }
}