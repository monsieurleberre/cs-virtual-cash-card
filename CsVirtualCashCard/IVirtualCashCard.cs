﻿namespace CsVirtualCashCard
{
    /// <remarks>
    /// For <see cref="TopUp"/> and <see cref="Withdraw"/>, returning a bool
    /// to let the user know of success or failure could be a good improvement.
    /// </remarks>
    public interface IVirtualCashCard
    {
        /// <summary>
        /// Returns the current balance on available on the card.
        /// </summary>
        decimal Balance { get; }
        
        /// <summary>
        /// Allows the user to top up the balance virtual card.
        /// </summary>
        /// <param name="amount">A positive value representing the amount by which the card's balance will be increased.</param>
        void TopUp(decimal amount);

        /// <summary>
        /// Allows the user to withdraw <see cref="amount"/> from the card if the balance allows it and the correct pin is provided.
        /// </summary>
        /// <param name="pin"></param>
        /// <param name="amount"></param>
        void Withdraw(string pin, decimal amount);
    }
}
